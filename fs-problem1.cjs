/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
const fs = require('fs');
const path = require('path');

function callback(err, data) {
    if(err) {
        console.log(err);
    } else {
        console.log(data);
    }
}

// function createRandomFile(filePath, fileData, callbackFn) {
//     fs.writeFile(filePath, fileData, (err) => {
//         callbackFn(err);
//     });
// }

function deleteRandomFiles(absoluteRandomDirPath, fileNamesArray) { 
    for(let index = 0; index < fileNamesArray.length; index ++) {

        const pathToDelete = path.join(absoluteRandomDirPath, fileNamesArray[index]);

        fs.unlink(pathToDelete, function (error) {
            if(error) {
                console.log('error while deleting:', error);
            } else {
                console.log(`file ${fileNamesArray[index]} successfully deleted`);
            }
        });
    } 
}

function main(absoluteRandomDirPath, numberOfFiles) {
    fs.mkdir(absoluteRandomDirPath, function (error) {
        if(error) {
            console.log('error while creating directory:', error);
        } else {
            let completedExecutions = 0;
            const fileNamesArray = [];
            for(let num = 1 ; num <= numberOfFiles; num ++) {

                const fileName = `file${num}.json`;
                const filePath = path.join(absoluteRandomDirPath, fileName);
                const fileData = {
                    id: num + Math.random() ,
                    name: `File ${num}`
                };

                fs.writeFile(filePath, JSON.stringify(fileData), (err) => {
                    if(err) {
                        callback(err);
                    } else {
                        completedExecutions += 1;
                        console.log(`file ${fileName} created.`);
                        fileNamesArray.push(fileName);
    
                        if(completedExecutions == numberOfFiles) {
                            // proceed with deletion
                            // const deleteDirPath = path.resolve('./random_json');
                            deleteRandomFiles(absoluteRandomDirPath, fileNamesArray);
                        }
                    }
                });
            }
        }
    })
}

module.exports = main;