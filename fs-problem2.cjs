/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require('fs');
const path = require('path');
const toReadFrom = path.join(__dirname, './lipsum (1).txt');

function solver() {
    
    // 1) reading the given file
    fs.readFile( toReadFrom, 'utf-8', function (error, data) {
        if(error) {
            console.log('error while reading file:', error);
            return;
        }
        // console.log(data);
        console.log(`data has been read from ${toReadFrom}`);
    
        // 2) writing the date to a new file after upper case conversion
        fs.writeFile('./toUpperCase.txt', data.toUpperCase(), function (error) {
            if(error) {
                console.log('error while writing after converting to uppercase:', error);
                return;
            }
            console.log('data has been written to new file after uppercase conversion');
    
            // storing the name of the file to which we just wrote into filesnames.txt
            fs.appendFile('./filenames.txt', 'toUpperCase.txt\n', (err) => {
                if (err) {
                  console.error(err);
                  return;
                }
                
                // 3) reading the new file, converting into lower case, spliting the sentenses and writing to another file
                fs.readFile('./toUpperCase.txt', 'utf-8', function (error, uppercaseData) {
                    if(error) {
                        console.log('error while reading upper case data:', error);
                        return;
                    }
    
                    const lowerCaseData = uppercaseData.toLowerCase();
                    const splitSentences = lowerCaseData.split('. ');
                    
                    // writing the split sentences to a new file
                    fs.writeFile('./splitContent.txt', JSON.stringify(splitSentences), function (err) {
                        if(err) {
                            console.log('error while writing split content:', err);
                            return;
                        }
                        console.log('successfuly written split content into new file');
    
                        // storing the name into filenames.txt
                        fs.appendFile('./filenames.txt', 'splitContent.txt\n', function (error) {
                            if(error) {
                                console.log('error while appending file name:', error);
                                return;
                            }
    
                            // 4) reading the new file, sorting it's content, writing the sorted content to a new file & storing it's name to filenames.txt
                            fs.readFile('./splitContent.txt', 'utf-8', function (error, splitContent) {
                                if(error) {
                                    console.log('error while reading split contents:', error);
                                    return;
                                }
    
                                const sortedContent = JSON.parse(splitContent).sort();
    
                                // writing the sorted data into a new file, storing its name into filenames.txt
                                fs.writeFile('./sorted.txt', JSON.stringify(sortedContent), function (error) {
                                    if(error) {
                                        console.log('error while writing the sorted data:', error);
                                        return;
                                    }
    
                                    // storing the name into filenames.txt
                                    fs.appendFile('./filenames.txt', 'sorted.txt\n', function (error) {
                                        if(error) {
                                            console.log('error while appending the file name:', error);
                                            return;
                                        }
    
                                        // 5) reading all the file names from filenmaes.txt and deleting them simultaneously
                                        fs.readFile('./filenames.txt', 'utf-8', function (error, fileNames) {
                                            if(error) {
                                                console.log('error while reading file names:', error);
                                                return;
                                            }
                                            // we are having an empty line in the filenames.txt because of '\n' whith the last name
                                            fileNmaesArr = fileNames.split('\n').filter(Boolean);
                                            // deleting the previously created files
                                            fileNmaesArr.map((fileName) => {
                                                fs.unlink(fileName, function (error) {
                                                    if(error) {
                                                        console.log('error while deleting the files:', error);
                                                        return;
                                                    }
                                                    console.log(`deleted ${fileName}`);
                                                });
                                            });
                                        });
                                    });
                                });
    
                            });
                        });
                    });
                });
            });
        });
    });
}

module.exports = solver;